#!/bin/bash

sudo gpasswd jenkins docker
sudo newgrp docker  #or usermod -aG docker jenkins
sudo service docker restart
sudo service jenkins restart
