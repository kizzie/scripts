#!/bin/bash

# https://wiki.jenkins-ci.org/display/JENKINS/Installing+Jenkins+on+Ubuntu
# on 12 you get the old version of jenkins
# make sure you are on 14.04 +

#get the jenkins repo
wget -q -O - https://jenkins-ci.org/debian/jenkins-ci.org.key | sudo apt-key add -
sudo sh -c 'echo deb http://pkg.jenkins-ci.org/debian binary/ > /etc/apt/sources.list.d/jenkins.list'
sudo apt-get update

#install git otherwise you will forget
sudo apt-get install -y git

#install and start jenkins
sudo apt-get install -y jenkins
service jenkins start
