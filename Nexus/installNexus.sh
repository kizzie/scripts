#!/bin/bash

#Needs Java 1.8
sudo yum install -y java-1.8.0-openjdk-devel.x86_64

#get and unzip file
curl -o nexus.tar.gz -L http://download.sonatype.com/nexus/3/latest-unix.tar.gz
tar -xvf nexus.tar.gz

#start service - port 8081
./nexus-3.2.0-01/bin/nexus start
