#!/bin/bash

# Add user 'chef', password 'chef'
useradd chef
echo chef | passwd chef --stdin

# Config sudo access. Assumes this is already configured in '/etc/ssh/sshd_config'
# #PasswordAuthentication yes
echo "chef    ALL=(ALL)       ALL" >> /etc/sudoers

#configure ssh access -- these two lines are new!

# centos 6
#sed -i 's/#PasswordAuthentication no/PasswordAuthentication yes/g' /etc/ssh/sshd_config
#service sshd restart

#centos 7
sed -i 's/#PasswordAuthentication yes/PasswordAuthentication yes/g' /etc/ssh/sshd_config
service sshd restart

#centos 6.5 -- it already lets you have passwords? don't bother with anything 
# ami - CentOS 6-5 -x86_64- - Release Media-6-5 - 2013-12-01-

# Flush & Disable iptables
iptables --flush
/sbin/chkconfig iptables off

# Disable SELinux
echo 0 > /selinux/enforce
sed -i "s/SELINUX=enforcing/SELINUX=disabled/g" /etc/selinux/config

# Set the MOTD
curl https://s3.amazonaws.com/uploads.hipchat.com/7557/343581/8r8bE9vCpEW87E8/getawesome.txt > /etc/motd


#DOwnload chefdk installer
curl https://opscode-omnibus-packages.s3.amazonaws.com/el/6/x86_64/chefdk-0.10.0-1.el6.x86_64.rpm -o chefdk-0.10.0-1.el6.x86_64.rpm
rpm -Uvh chefdk-0.10.0-1.el6.x86_64.rpm