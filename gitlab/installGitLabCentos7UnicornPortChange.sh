#!/bin/bash
#should be installed already, but in case
yum install -y openssh-server
yum install -y ca-certificates

#get gitlab
yum install curl policycoreutils openssh-server openssh-clients
systemctl enable sshd
systemctl start sshd
yum install -y postfix
systemctl enable postfix
systemctl start postfix
# firewall-cmd --permanent --add-service=http
# systemctl reload firewalld

# curl -sS https://packages.gitlab.com/install/repositories/gitlab/gitlab-ce/script.rpm.sh | sudo bash
yum install -y wget
wget https://packages.gitlab.com/install/repositories/gitlab/gitlab-ce/script.rpm.sh
chmod +x script.rpm.sh
./script.rpm.sh

yum install -y gitlab-ce

_myip="$(curl http://169.254.169.254/latest/meta-data/public-ipv4)"
sed -i "/^external_url/s/external_url.*/external_url 'http:\/\/${_myip}'/" /etc/gitlab/gitlab.rb
sed -i '/^external_url/a unicorn["port"] = 8889' /etc/gitlab/gitlab.rb

gitlab-ctl reconfigure
