#!/bin/bash

#should be installed already, but in case
apt-get install -y openssh-server
apt-get install -y ca-certificates

#setup the default answers, change the name to whatever yours is
_myDNS = "$(curl http://169.254.169.254/latest/meta-data/public-hostname)"
debconf-set-selections <<< "postfix postfix/mailname string ${_myDNS}"
debconf-set-selections <<< "postfix postfix/main_mailer_type string 'Internet Site'"
apt-get install -y postfix

#get gitlab
curl https://packages.gitlab.com/install/repositories/gitlab/gitlab-ce/script.deb.sh | sudo bash
apt-get install -y gitlab-ce

_myip="$(curl http://169.254.169.254/latest/meta-data/public-ipv4)"
sed -i "/^external_url/s/external_url.*/external_url 'http:\/\/${_myip}'/" /etc/gitlab/gitlab.rb
sed -i '/^external_url/a unicorn["port"] = 8889' /etc/gitlab/gitlab.rb
gitlab-ctl reconfigure
