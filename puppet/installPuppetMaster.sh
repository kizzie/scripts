#!/bin/bash

# sort the hostname - based on getting the IP from aws
hostname master
echo master > /etc/hostname
_myip="$(curl http://169.254.169.254/latest/meta-data/public-ipv4)"
echo ${_myip} master master.acme.com >> /etc/hosts

# get the installation file
wget https://s3-eu-west-1.amazonaws.com/puppet-installers/puppet-enterprise-3.7.1-ubuntu-14.04-amd64.tar.tar
tar  -xvf puppet-enterprise-3.7.1-ubuntu-14.04-amd64.tar.tar

# run the file
cd puppet-enterprise-3.7.1-ubuntu-14.04-amd64
./puppet-enterprise-installer
