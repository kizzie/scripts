#!/bin/bash

#sort key
sudo apt-key del puppet
wget -O - https://downloads.puppetlabs.com/puppetlabs-gpg-signing-key.pub | gpg --import
wget --quiet -O - https://downloads.puppetlabs.com/puppetlabs-gpg-signing-key.pub | sudo apt-key add -

#installs puppet master using generic details from aws
wget  https://s3-eu-west-1.amazonaws.com/puppet-installers/puppet-enterprise-3.7.1-ubuntu-14.04-amd64.tar.tar
tar  -xvf puppet-enterprise-3.7.1-ubuntu-14.04-amd64.tar.tar
yes | sudo ./puppet-enterprise-3.7.1-ubuntu-14.04-amd64/puppet-enterprise-installer
