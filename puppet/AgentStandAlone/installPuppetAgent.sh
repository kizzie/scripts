#!/bin/bash

sudo apt-get install wget

wget https://apt.puppetlabs.com/puppetlabs-release-pc1-trusty.deb
sudo dpkg -i puppetlabs-release-pc1-trusty.deb

apt-get install puppet

service puppet start
