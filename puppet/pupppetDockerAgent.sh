#!/bin/bash

#install puppetAgent
./puppetAgent.sh

#docker module
puppet module install garethr/docker

#check in with the master
puppet agent -t
