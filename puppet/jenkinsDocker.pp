class jenkins {
    include 'docker'

    docker::image { "jenkins":
        image_tag => "latest",
    }

    docker::run { 'Team1':
        image       => "jenkins",
        ports       => ["80:8080"],
    }
}
