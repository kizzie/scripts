class aws {

  ec2_securitygroup { 'puppets':
  ensure      => present,
  region      => 'eu-west-1',
  description => 'ports required for webserver',
  ingress     => [{
    protocol  => 'tcp',
    port      => 80,
    cidr      => '0.0.0.0/0',
  },{
    protocol  => 'tcp',
    port      => 22,
    cidr      => '0.0.0.0/0',
  }],
}

ec2_instance { 'puppet-created':
  ensure       	=> present,
  region        	=> 'eu-west-1',
  image_id    	=> 'ami-ed82e39e',
  instance_type    	=> 't2.micro',
  key_name 	 	=> 'qwikLABS-L582-27770.pem',
  security_groups   	=> ['puppets'],
  subnet => 'name'
}

}
