#!/bin/bash

wget -O puppetinstaller.tar.gz 'https://pm.puppetlabs.com/cgi-bin/download.cgi?dist=ubuntu&rel=16.04&arch=amd64&ver=latest'
tar -xvf puppetinstaller.tar.gz

cd puppet-enterprise-2016.*
sudo puppet-enterprise-installer -a pe.conf
