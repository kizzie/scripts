#!/bin/bash

#sort key
sudo apt-key del puppet
wget -O - https://downloads.puppetlabs.com/puppetlabs-gpg-signing-key.pub | gpg --import
wget --quiet -O - https://downloads.puppetlabs.com/puppetlabs-gpg-signing-key.pub | sudo apt-key add -

hostname agent1.acme.com
echo webserver1 > /etc/hostname

echo `curl http://169.254.169.254/latest/meta-data/local-ipv4`  webserver1 >> /etc/hosts

#need to pick up the IP from somewhere, too hardcoded for my liking!
echo [puppet-master=ip] puppet  >> /etc/hosts

curl -k https://puppet:8140/packages/current/install.bash | sudo bash
