#!/bin/bash

echo 'removing old files'
aws s3 rm s3://qadevops/QADOCKERINT/setup.sh

echo 'pushing new files'
aws s3 cp setup.sh s3://qadevops/QADOCKERINT/ --grants read=uri=http://acs.amazonaws.com/groups/global/AllUsers

echo 'building stack'
aws cloudformation create-stack --stack-name docker --template-body file://cloudformation.template
