import sys

#read in top and bottom
top  = open('top.template', 'r')
bottom  = open('bottom.template', 'r')

#generate number of resources
#get number of instances
if len(sys.argv) > 1:
   number = int(sys.argv[1])
   print "Creating " + str(number) +  " machines"
else:
   print "No number specified, creating one machine"
   number = 1

#loop add to string
middle = ""
for x in range(0, number):
   middle += "\"Student" + str(x+1) + "\" : { \n "
   middle += "\"Type\" : \"AWS::EC2::Instance\", \n"
   middle += "\"Properties\" : { \n"
   middle += "\"InstanceType\" : \"m4.large\",\n"
   middle += "\"SecurityGroups\" : [ { \"Ref\" : \"SecurityGroupName\" } ], \n "
   middle += "\"Tags\" : [{\"Key\" : \"Name\",\"Value\" : \"Student" + str(x) + "\"}], \n"
   middle += "\"KeyName\" : { \"Ref\" : \"KeyName\" }, \n"
   middle += "\"ImageId\" : { \"Fn::FindInMap\" : [ \"RegionMap\", { \"Ref\" : \"Region\" }, \"AMI\" ]} \n"
   middle += "  } \n}"
   if x == number-1:
       middle += "\n"
   else:
       middle += ", \n"
   print(x)

#output all
output = open ('final.template', 'w')
output.write(top.read())
output.write(middle)
output.write(bottom.read())
output.close()
