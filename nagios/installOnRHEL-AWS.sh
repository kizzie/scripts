#!/bin/bash

#install nagios xi on redhat linux on aws
yum update -y
yum install -y epel-release wget
yum-config-manager --enable rhui-REGION-rhel-server-extras rhui-REGION-rhel-server-optional

cd /tmp
wget http://assets.nagios.com/downloads/nagiosxi/xi-latest.tar.gz
tar xzf xi-latest.tar.gz

cd /tmp/nagiosxi
touch installed.firewall

sed -i '77 s/^/#/' 0-repos

yes | sudo ./fullinstall
