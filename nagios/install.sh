#!/bin/bash

sudo yum update -y

#because sometimes we don't have it...
sudo yum install -y wget

#required for pre-reqs in setup
#sudo yum install -y epel-release

#required for step 6 of the install
#sudo yum install -y firewalld
#sudo systemctl enable firewalld

#actual install, still need to select Y to continue in full install step...
wget http://assets.nagios.com/downloads/nagiosxi/xi-latest.tar.gz 
tar xzf xi-latest.tar.gz 
cd nagiosxi
sudo ./fullinstall <<< 'y'