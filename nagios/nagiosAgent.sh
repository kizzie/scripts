#!/bin/bash

# https://assets.nagios.com/downloads/nagiosxi/docs/Monitoring_Hosts_Using_NRPE.pdf
# https://assets.nagios.com/downloads/nagiosxi/docs/Installing_The_XI_Linux_Agent.pdf


sudo yum install -y wget
#again, setup the firewall, because.... nagios.
#sudo yum install -y firewalld
#sudo systemctl enable firewalld
#should do the same as previous line, but seems to actually work
#sudo service firewalld start


wget http://assets.nagios.com/downloads/nagiosxi/agents/linux-nrpe-agent.tar.gz
tar xzf linux-nrpe-agent.tar.gz
cd linux-nrpe-agent
sudo ./fullinstall # <<<'y'

#need to give it the ip address of the master

#next add in from the server side:
	# configure
	# run monitoring wizard
	# linux server
	# add ip address
	# go through to end
	# click finish

# to enable ping - new icmp rule - echo request from anywhere
