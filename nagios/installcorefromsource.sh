#!/bin/bash

#install pre-req
yum install wget httpd php gcc glibc glibc-common gd gd-devel make net-snmp -y

#get the file
wget http://prdownloads.sourceforge.net/sourceforge/nagios/nagios-4.0.8.tar.gz

#add a new user for nagios
useradd nagios

#untar the file and install
tar -xvzf nagios-4.0.8.tar.gz
cd nagios-4.0.8
./configure
make all
make install
make install-init
make install-commandmode
make install-config
make install-webconf
make install-exfoliation

#configure
/usr/local/nagios/bin/nagios -v /usr/local/nagios/etc/nagios.cfg

#change the password to login
#for amazon linux remove the -B flag
#htpasswd -c -B -b /usr/local/nagios/etc/htpasswd.users nagiosadmin admin 
htpasswd -c -b /usr/local/nagios/etc/htpasswd.users nagiosadmin admin 

#add the plugins
yum install -y epel-release
yum install nagios-plugins-all -y

#remove old libexec
rm -rf /usr/local/nagios/libexec

#link the new one
ln -s /usr/lib64/nagios/plugins/ /usr/local/nagios/libexec
chown -R nagios:nagios /usr/local/nagios/libexec/

#enable all the services
#for amazon linux change this to: sudo service httpd start
#systemctl enable httpd
#systemctl start httpd
#chkconfig nagios on
sudo service httpd start
/etc/init.d/nagios start
