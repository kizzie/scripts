#!/bin/bash

if [[ $(docker ps -q) ]]; then
  docker stop $(docker ps -q)
else
  echo 'no containers to stop'
fi
