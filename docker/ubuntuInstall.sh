#!/bin/bash

#Ubuntu Setup for Docker

#update all
sudo apt-get update -y
sudo apt-get install -y curl 

#get docker 
curl -sSL https://get.docker.com/ | sh

#start service
sudo service docker start