#!/bin/bash

echo 'removing old files'
aws s3 rm s3://qadevops/nagiosxi/installOnRHEL.sh

echo 'adding new files'
aws s3 cp files/installOnRHEL.sh s3://qadevops/nagiosxi/ --grants read=uri=http://acs.amazonaws.com/groups/global/AllUsers

echo 'creating stack'
aws cloudformation create-stack --stack-name  nagios --template-body file://nagios.template

echo 'done'
