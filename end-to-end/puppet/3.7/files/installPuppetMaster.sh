#!/bin/bash

sudo apt-key del puppet
wget -O - https://downloads.puppetlabs.com/puppetlabs-gpg-signing-key.pub | gpg --import
wget --quiet -O - https://downloads.puppetlabs.com/puppetlabs-gpg-signing-key.pub | sudo apt-key add -

#installs puppet master using generic details from aws
wget  https://s3-eu-west-1.amazonaws.com/puppet-installers/puppet-enterprise-3.7.1-ubuntu-14.04-amd64.tar.tar
tar  -xvf puppet-enterprise-3.7.1-ubuntu-14.04-amd64.tar.tar

#get the answers file
wget https://s3-eu-west-1.amazonaws.com/qa-devops/puppet/puppet.answers

#get the current public IP
_myip="$(curl http://169.254.169.254/latest/meta-data/public-ipv4)"

#use sed to replace master.acme.com with the public ip
sed -i "s/master.acme.com/${_myip}/g" puppet.answers

yes | sudo ./puppet-enterprise-3.7.1-ubuntu-14.04-amd64/puppet-enterprise-installer -a puppet.answers
