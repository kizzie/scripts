#!/bin/bash

hostname webserver1
echo webserver1 > /etc/hostname

echo `curl http://169.254.169.254/latest/meta-data/local-ipv4`  webserver1 `curl http://169.254.169.254/latest/meta-data/hostname`  >> /etc/hosts

echo ${_MasterIP} master puppet >>/etc/hosts

#wait for the server to respond!
waiting=true
while [[ $waiting ]] ; do
  if [[ $(curl -k https://puppet:8140/packages/current/install.bash) ]]; then break; fi
  sleep 2m
done
echo 'server ready'

curl -k https://puppet:8140/packages/current/install.bash | sudo bash

puppet agent -t
