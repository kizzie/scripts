#!/bin/bash

#installs puppet master using generic details from aws
wget  https://s3-eu-west-1.amazonaws.com/puppet-installers/puppet-enterprise-3.7.1-ubuntu-14.04-amd64.tar.tar
tar  -xvf puppet-enterprise-3.7.1-ubuntu-14.04-amd64.tar.tar

#get the answers file
wget https://s3-eu-west-1.amazonaws.com/qadevops/puppet/puppet.answers

#get the current public IP
#use the private DNS instead
_myhostname="$(curl http://169.254.169.254/latest/meta-data/hostname)"

#use sed to replace master.acme.com with the public ip
sed -i "s/master.acme.com/${_myhostname}/g" puppet.answers

yes | sudo ./puppet-enterprise-3.7.1-ubuntu-14.04-amd64/puppet-enterprise-installer -a puppet.answers
