#!/bin/bash
apt-get update
apt-get upgrade

wget -O puppetinstaller.tar.gz 'https://s3-eu-west-1.amazonaws.com/qa-devops/puppet/installers/puppet-enterprise-2016.5.1-ubuntu-16.04-amd64.tar.gz'
tar -xvf puppetinstaller.tar.gz

cd puppet-enterprise-2016.*

cat << EOT >> pe.conf
{
  "console_admin_password": "password",
  "puppet_enterprise::puppet_master_host": "LOCAL-DNS",
  "pe_install::puppet_master_dnsaltnames": [
    "puppet"
  ]
}
EOT

sed -i "s/LOCAL-DNS/`curl http://169.254.169.254/latest/meta-data/local-hostname`/g" pe.conf

./puppet-enterprise-installer -c pe.conf

puppet agent -t
