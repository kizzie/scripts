echo 'removing old files'
aws s3 rm s3://qadevops/puppet/puppet.answers
aws s3 rm s3://qadevops/puppet/installPuppetMaster.sh
aws s3 rm s3://qadevops/puppet/installPuppetAgent.sh

echo 'pushing new files'
aws s3 cp files/puppet.answers s3://qadevops/puppet/ --grants read=uri=http://acs.amazonaws.com/groups/global/AllUsers
aws s3 cp files/installPuppetMaster.sh s3://qadevops/puppet/ --grants read=uri=http://acs.amazonaws.com/groups/global/AllUsers
aws s3 cp files/installPuppetAgent.sh s3://qadevops/puppet/ --grants read=uri=http://acs.amazonaws.com/groups/global/AllUsers


# aws cloudformation create-stack --stack-name puppetmaster --template-body file://puppetmaster.template

echo 'creating stack'
aws cloudformation create-stack --stack-name puppet --template-body file://puppetmasterandagent.template
