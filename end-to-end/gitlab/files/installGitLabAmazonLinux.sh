#!/bin/bash

#change the hostname to the external dns
sudo hostname $(curl http://169.254.169.254/latest/meta-data/public-hostname)

#install the dependencies.
sudo yum install -y curl openssh-server postfix cronie
sudo service postfix start
sudo chkconfig postfix on

#add the rpm and install gitlab
curl -s https://packages.gitlab.com/install/repositories/gitlab/gitlab-ce/script.rpm.sh | sudo bash

sudo yum install -y gitlab-ce-8.5.0-ce.1.el6.x86_64

#tell it to reconfigure to set everything up
sudo gitlab-ctl reconfigure

#now import the existing setup
#copy file from s3 to server
wget https://s3-eu-west-1.amazonaws.com/qa-devops/gitlab/1456238258_gitlab_backup.tar
sudo mv 1456238258_gitlab_backup.tar /var/opt/gitlab/backups/

#stop all
sudo gitlab-ctl stop unicorn
sudo gitlab-ctl stop sidekiq

#restore backup
sudo gitlab-rake gitlab:backup:restore BACKUP=1456238258 force=yes

#start all
sudo gitlab-ctl start
